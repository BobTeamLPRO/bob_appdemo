package com.danielgf.bobproof;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    final int port = 9998;
    int portBobadcast = 9995;
    InetAddress ourIP;
    InetAddress BroadcastIP;
    String bobIP = "192.168.2.2";

    AlertDialog alertDialog;

    String messageUDPProtocol = "app*connect";

    byte[] messageReceived;

    static DatagramSocket socketUDP;

    DatagramPacket datSended;
    DatagramPacket datReceived;

    private Button sendTextButton; // Botón de enviar
    private EditText plainText; // Texto a enviar
    private ImageView bobLogo; // Logo de Bob

    Socket connectSocket; // Socket que utilizaremos en la petición
    String textToSend; // String que contendrá el texto que enviemos

    // Atributos necesarios para los toast

    Context context;
    CharSequence textConnected;
    CharSequence textBusy;
    CharSequence textError;
    int duration;

    CharSequence textIPGet;
    CharSequence textIPNotGet;

    Toast toastConnected;
    Toast toastBusy;
    Toast toastError;
    Toast toastIPGet;
    Toast toastIPNotGet;

    boolean connectedBoolean = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendTextButton = (Button) findViewById(R.id.buttonSend);
        plainText = (EditText) findViewById(R.id.editTextSend);
        bobLogo = (ImageView) findViewById(R.id.bobLogo);

        sendTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SendText().execute(); // En el caso de pulsar en enviar, ejecutaremos un objeto nuevo de la clase EnviarTexto

            }
        });

        bobLogo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new BobIP().execute();

            }
        });

    }

    private class SendText extends AsyncTask<Void, Void, Integer> {

        boolean bobFound;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            bobFound = true;

            try {

                textToSend = plainText.getText().toString(); // Obtenemos el texto a enviar escrito

                // Creamos los distintos Toast

                context = getApplicationContext();
                textConnected = "¡Bob ha recibido tu mensaje!";
                textBusy = "Bob está ocupado. Prueba en un rato.";
                textError = "Bob no está disponible. Prueba en un rato.";
                duration = Toast.LENGTH_SHORT;

                toastConnected = Toast.makeText(context,textConnected,duration);
                toastBusy = Toast.makeText(context,textBusy,duration);
                toastError = Toast.makeText(context,textError,duration);

            } catch (Exception e) {

                Log.d("Excepción", "Ha ocurrido una excepción: " + e);

                Context context = getApplicationContext();
                CharSequence text = e.getMessage();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }

        }

        @Override
        protected Integer doInBackground(Void... params) {

            if (!textToSend.equals("")) {

                try {

//                    connectSocket = new Socket(bobIP, port); // Establecemos el Socket

                    connectSocket = new Socket();

                    connectSocket.connect(new InetSocketAddress(bobIP,port), 1000);

                    if (connectSocket.isConnected()) { // Si conseguimos conectarnos

                        toastConnected.show(); // Mostramos que nos hemos conectado

                        connectedBoolean = true;

                        textToSend = "notify*demo*" + textToSend;

                        Log.d("Enviado","" + textToSend);

                        OutputStream flujoSalida = connectSocket.getOutputStream(); // Obtenemos el flujo de salida

                        flujoSalida.write(textToSend.getBytes()); // Enviamos por el flujo de salida el texto como si fuera un objeto

                    } else {

                        bobFound = false;

                        toastBusy.show();

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    bobFound = false;

                    toastError.show();

                }


                return 0;

            }

            else {

                return -1;

            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (bobFound) {

                bobLogo.setImageResource(R.drawable.logo_bob);

            }

            else {

                bobLogo.setImageResource(R.drawable.logo_bob2);

            }

            if (connectedBoolean) {

                alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setTitle("¡Espera!");
                alertDialog.setMessage("Bob es un prototipo y no quiere que hagas spam, así que deberás esperar unos segundos para volver a mandarle algo.");
                alertDialog.show();

                connectedBoolean = false;

                final Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        alertDialog.dismiss();
                    }
                }, 5000);

            }

        }
    }

    private class BobIP extends AsyncTask<Void, Void, Integer> {

        boolean bobFound;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            bobFound = false;

            messageReceived = new byte[1024];

            getOurIPs();

            alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle("¡Espera!");
            alertDialog.setMessage("Estamos intentando conectar con Bob. Serán solo unos segundos.");
            alertDialog.show();

            try {

                context = getApplicationContext();
                textIPGet = "¡Ya sabemos dónde está Bob!";
                textIPNotGet = "No es posible encontrar a Bob. Prueba en un rato.";
                duration = Toast.LENGTH_SHORT;

                toastIPGet = Toast.makeText(context,textIPGet,duration);
                toastIPNotGet = Toast.makeText(context,textIPNotGet,duration);

            } catch (Exception e) {

                Log.d("Excepción", "Ha ocurrido una excepción: " + e);

                Context context = getApplicationContext();
                CharSequence text = e.getMessage();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }


        }

        @Override
        protected Integer doInBackground(Void... params) {

            try {

                socketUDP = new DatagramSocket(portBobadcast,ourIP);

                socketUDP.setBroadcast(true);

                Log.d("Message", messageUDPProtocol + "; length: " + messageUDPProtocol.length() + " ");

                datSended = new DatagramPacket(messageUDPProtocol.getBytes(),messageUDPProtocol.length(),BroadcastIP,portBobadcast);

                datReceived = new DatagramPacket(messageReceived,messageReceived.length);

                socketUDP.send(datSended);

                while (true) {

                    socketUDP.setSoTimeout(5000);

                    socketUDP.receive(datReceived);

                    String messageReceived = new String(datReceived.getData()).trim();

                    if (messageReceived.equals("discoverybob")) {

                        bobIP = datReceived.getAddress().getHostAddress();

                        bobFound = true;

                        alertDialog.dismiss();

                        toastIPGet.show();

                        socketUDP.disconnect();

                        socketUDP.close();

                        break;

                    }

                }

            } catch (Exception e) {

                e.printStackTrace();

                socketUDP.disconnect();

                socketUDP.close();

                alertDialog.dismiss();

                toastIPNotGet.show();

            }

            return -1;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (bobFound) {

                bobLogo.setImageResource(R.drawable.logo_bob);

            }

            else {

                bobLogo.setImageResource(R.drawable.logo_bob2);

            }
        }
    }

    private void getOurIPs() {

        try {

            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {

                for (InterfaceAddress interfaceAddress : intf.getInterfaceAddresses()) {

                    InetAddress auxBroadcastIP = interfaceAddress.getBroadcast();

                    InetAddress auxIP = interfaceAddress.getAddress();

                    if ((auxIP instanceof Inet4Address) && !auxIP.isLoopbackAddress() && (auxBroadcastIP != null)) {

                        BroadcastIP = auxBroadcastIP;

                        ourIP = auxIP;

                        Log.d("Addresses","IP: " + ourIP.getHostAddress() + "; Broadcast: " + BroadcastIP.getHostAddress());

                    }
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
